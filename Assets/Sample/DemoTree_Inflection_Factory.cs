﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoTree_Inflection_Factory : ITreeFactory
{
    public  const  int  MoveID = 1;
    protected override BeTreeNode DoCreatTree()
    {
        Debug.Log("create tree");

        var root = new BeTreeNodePrioritySelector();
        var move = new BeTreeNodeParallel(BeTreeNodeParallel.LogicGateEnum.AND);
        var moveTo = new BeTreeNodeSequence();
        root.AddChild(new Precondition(new RandomTime(3, 5),
                      new NodeNewTarget()));
        root.AddChild(new PreconditionNOT(new IsReachTarget(), moveTo));
            moveTo.AddChild( new NodeRotate());
            moveTo.AddChild(move);
                move.AddChild(new BeTreeNodeInflectionLeaf(MoveID));
                move.AddChild(new NodeBreath());
        root.AddChild(new NodeIdle());
        
        return root;
    }
}
