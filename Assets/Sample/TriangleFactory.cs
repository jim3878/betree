﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleFactory : MonoBehaviour {

    public GameObject triangle;
    public GameObject target;
    public int max = 5;
	// Use this for initialization
	void Start () {
		for(int i = 0; i < max; i++)
        {
            Creat();
        }
	}

    [ContextMenu("creat")]
    void Creat()
    {
        var go = Instantiate(this.triangle);
        var triangle = go.AddComponent<TriangleBehaviour>();
        var target = Instantiate(this.target);
        triangle.triangle = go.transform;
        triangle.target = target.transform;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Creat();
        }
    }
}
