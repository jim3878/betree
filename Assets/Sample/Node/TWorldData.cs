﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TWorldData : WorkData
{
       
    public float inhaleSize = 1.5f;
    public float exhaleSize = 0.5f;
    public float randomDeltaTime = 0.5f;
    public float breathSpeed = 2f;
    public float moveSpeed = 2f;
    public float rotateSpeed = 100f;
    public Transform triangle { get; set; }
    public Transform target { get; set; }
}
