﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeIdle : BeTreeNodeLeaf
{
    protected override NodeStatusEnum Update(WorkData input)
    {
        var data = input as TWorldData;
        var userData = GetUserData<IdleData>(input);
        if (Time.time - data.randomDeltaTime > userData.lastTimeMark)
        {
            userData.lastTimeMark = Time.time;
            //var c=new Color()
            data.triangle.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0, 256) / 255f, Random.Range(0, 256) / 255f, Random.Range(0, 256) / 255f);
        }
        return NodeStatusEnum.RUNNING;
    }
}

public class IdleData
{
    public float lastTimeMark = Time.time;
}