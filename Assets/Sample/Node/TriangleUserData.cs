﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleUserData : MonoBehaviour {

    public Color color;
    public float _time;
    
    
    public TriangleUserData()
    {
        RandomColor();
        _time = Time.time;
    }

    public void RandomColor()
    {
        color = new Color(Random.Range(0, 256) / 255, Random.Range(0, 256) / 255, Random.Range(0, 256) / 255);
    }
}
