﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeBreath : BeTreeNodeLeaf
{
    protected override NodeStatusEnum Update(WorkData input)
    {
        //Debug.Log("breathe");
        var breathData = GetUserData<BreathStatue>(input);
        var data = input as TWorldData;
        //Debug.Log(string.Format("scale {0}  targetScale {1}", data.triangle.localScale, breathData.targetSize));
        data.triangle.localScale = Vector3.MoveTowards(data.triangle.localScale, breathData.targetSize, data.breathSpeed * Time.deltaTime);

        if (data.triangle.localScale == breathData.targetSize)
        {
            breathData.status = breathData.status == BreathStatue.StatusEnum.EXHALE ? BreathStatue.StatusEnum.INHALE : BreathStatue.StatusEnum.EXHALE;
            switch (breathData.status)
            {
                case BreathStatue.StatusEnum.EXHALE:
                    breathData.targetSize = Vector3.one * data.exhaleSize;
                    break;
                case BreathStatue.StatusEnum.INHALE:
                    breathData.targetSize = Vector3.one * data.inhaleSize;
                    break;
            }
        }

        return NodeStatusEnum.RUNNING;
    }

    protected override void Exit(WorkData input, NodeStatusEnum status)
    {
        var breathData = GetUserData<BreathStatue>(input);
        var data = input as TWorldData;
        data.triangle.localScale = Vector3.one;
    }
}

public class BreathStatue
{
    public enum StatusEnum
    {
        EXHALE = 1,
        INHALE = -1
    }
    public StatusEnum status = StatusEnum.EXHALE;
    public Vector3 targetSize = Vector3.one*0.5f;

}