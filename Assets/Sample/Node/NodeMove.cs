﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeMove : BeTreeNodeLeaf
{
    protected override NodeStatusEnum Update(WorkData input)
    {
        //Debug.Log("move");
        var data = input as TWorldData;
        data.triangle.position = Vector3.MoveTowards(data.triangle.position, data.target.position, data.moveSpeed*Time.deltaTime);
        if (data.triangle.position == data.target.position)
        {
            return NodeStatusEnum.FINISH;
        }
        return NodeStatusEnum.RUNNING;
    }
}
