﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeNewTarget : BeTreeNodeLeaf {

    protected override NodeStatusEnum Update(WorkData input)
    {
        var data = input as TWorldData;
        
        data.target.transform.position = new Vector3(Random.Range(8, -8), Random.Range(4, -4), 0);
        //Debug.Log(data.target.transform.position);
        return NodeStatusEnum.FINISH;
    }
}
