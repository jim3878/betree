﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeRotate : BeTreeNodeLeaf
{

    public NodeRotate() : base()
    {
    }

    protected override NodeStatusEnum Update(WorkData input)
    {
        var data = input as TWorldData;
        //Debug.Log("rotate");
        var rotationTarget = Quaternion.LookRotation(Vector3.forward, (data.target.position - data.triangle.position).normalized);
        data.triangle.rotation = Quaternion.RotateTowards(data.triangle.rotation, rotationTarget, data.rotateSpeed * Time.deltaTime);
        if (rotationTarget == data.triangle.rotation)
        {
            return NodeStatusEnum.FINISH;
        }
        else
        {
            return NodeStatusEnum.RUNNING;
        }

    }
}
