﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleBehaviour : MonoBehaviour
{

    static ITreeFactory tree;
    public Transform triangle;
    public Transform target;

    static FastMove fastMove;
    static SlowMove slowMove;

    TWorldData input;
    // Use this for initialization
    void Start()
    {
        if (tree == null)
        {
            tree = new DemoTree_Inflection_Factory();
        }
        input = new TWorldData
        {
            target = target,
            triangle = triangle
        };

        if (Random.Range(0, 100) > 50)
        {
            input.AddBehaviour<FastMove>(DemoTree_Inflection_Factory.MoveID);
        }
        else
        {
            input.AddBehaviour<SlowMove>(DemoTree_Inflection_Factory.MoveID);
        }
        
        //target.transform.position = new Vector3(Random.Range(4, -4), Random.Range(4, -4));
    }

    // Update is called once per frame
    void Update()
    {
        var root = tree.CreatTree();
        //Debug.Log(root.Evaluate(input));
        if (root.Evaluate(input))
        {
            root.Excute(input);
        }
    }
}
