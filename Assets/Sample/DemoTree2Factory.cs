﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoTree_2Factory : ITreeFactory
{
    protected override BeTreeNode DoCreatTree()
    {
        Debug.Log("create DemoTree_2Factory");

        var root = new BeTreeNodePrioritySelector();
        var breathe = new BeTreeNodeParallel(BeTreeNodeParallel.LogicGateEnum.OR);
            breathe.AddChild(new NodeBreath());
            breathe.AddChild(new PreconditionTRUE(root));

        var move = new BeTreeNodeParallel(BeTreeNodeParallel.LogicGateEnum.AND);
        var moveTo = new BeTreeNodeParallel(BeTreeNodeParallel.LogicGateEnum.OR);
            root.AddChild(new Precondition(new RandomTime(3, 5),
                          new NodeNewTarget()));
            root.AddChild(new PreconditionNOT(new IsReachTarget(), moveTo));
                moveTo.AddChild(new NodeRotate());
                moveTo.AddChild(move);
                    move.AddChild(new NodeMove());
        //moveTo.AddChild(new NodeBreath());
        //root.AddChild(new NodeIdle());

        return breathe;
    }
}
