﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsReachTarget : IPrecondition
{
    public override bool Condition(WorkData input)
    {
        var data = input as TWorldData;
        return data.target.position == data.triangle.position;
    }
}
