﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTime : IPrecondition
{
    float min, max;
    public RandomTime(float min, float max) : base()
    {
        this.min = min;
        this.max = max;
    }
    

    public override bool Condition(WorkData input)
    {
        //Debug.Log("wait Time "+ (Time.time - GetContext<WaitTime>(input).startTime));
        if (GetContext<WaitTime>(input).startTime == 0 || Time.time > GetContext<WaitTime>(input).startTime)
        {
            GetContext<WaitTime>(input).startTime = Time.time + UnityEngine.Random.Range(min, max);
            return true;
        }
        return false;
        //return Time.time > GetContext<WaitTime>(input).startTime;
    }
}

public class WaitTime : NodeData
{
    public float startTime = 0;
}