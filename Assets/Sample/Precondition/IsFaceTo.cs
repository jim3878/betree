﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsFaceTo : IPrecondition
{
    public override bool Condition(WorkData input)
    {
        var data = input as TWorldData;

        if(Mathf.Abs(Vector3.Dot((data.target.position - data.triangle.position).normalized, data.triangle.up.normalized) - 1) < 0.01f)
        {
            Debug.Log("face to");
        }else
        {
            Debug.Log("not face to " + Vector3.Dot((data.target.position - data.triangle.position).normalized, data.triangle.up.normalized));
        }
        //Debug.Log("faceTo" +Vector3.Dot((data.target.position - data.triangle.position).normalized, data.triangle.up.normalized));
        return Mathf.Abs(Vector3.Dot((data.target.position - data.triangle.position).normalized, data.triangle.up.normalized)- 1)<0.01f;
    }
}
