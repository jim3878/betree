﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ITreeFactory
{
    private BeTreeNode root;

    public BeTreeNode CreatTree()
    {
        if (root == null)
        {
            root = DoCreatTree();
        }
        return root;
    }
    protected abstract BeTreeNode DoCreatTree();
}
