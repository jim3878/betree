﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeTreeNodeParallel : BeTreeNode
{
    public enum LogicGateEnum
    {
        AND,
        OR,
    }

    private LogicGateEnum gate;

    public BeTreeNodeParallel( LogicGateEnum gate) : base(16)
    {
        this.gate = gate;
    }

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        switch (gate)
        {
            case LogicGateEnum.AND:
                return ANDExcute(input);
            case LogicGateEnum.OR:
                return ORExcute(input);
        }
        return NodeStatusEnum.ERROR;
    }

    private NodeStatusEnum ANDExcute(WorkData input)
    {
        NodeStatusEnum status = NodeStatusEnum.RUNNING;
        for (int i = 0; i < childList.Count; i++)
        {
            if (childList[i].Excute(input) == NodeStatusEnum.FINISH)
            {
                status = NodeStatusEnum.FINISH;
                this.Transition(input);
            }
        }
        return status;
    }

    private NodeStatusEnum ORExcute(WorkData input)
    {
        NodeStatusEnum status = NodeStatusEnum.FINISH;
        
        for (int i = 0; i < childList.Count; i++)
        {
            if (childList[i].Excute(input) == NodeStatusEnum.RUNNING)
            {
                status = NodeStatusEnum.RUNNING;
            }
        }
        if (status == NodeStatusEnum.FINISH)
        {
            this.Transition(input);
        }
        return status;
    }

    public override void Transition(WorkData input)
    {
        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].Transition(input);
        }
    }

    protected override bool DoEvalute(WorkData input)
    {
        for (int i = 0; i < childList.Count; i++)
        {
            if (!childList[i].Evaluate(input))
            {
                return false;
            }
        }
        return true;
    }
}
