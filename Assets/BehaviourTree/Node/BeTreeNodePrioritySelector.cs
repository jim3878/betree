﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeTreeNodePrioritySelector : BeTreeNode
{

    public BeTreeNodePrioritySelector() : base(16)
    {
    }

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        var context = GetContext<NodeDataPriority>(input);
        if (context.currNode == -1 || context.currNode >= childList.Count)
        {
            return NodeStatusEnum.FINISH;
            throw new Exception("Node is not exist");
        }
        return childList[context.currNode].Excute(input);
    }

    public override void Transition(WorkData input)
    {
        var context = GetContext<NodeDataPriority>(input);
        if (context.currNode == -1)
            return;
        if (context.currNode < -1 || context.currNode >= childList.Count)
        {
            throw new Exception("Transition Node not exist");
        }
        childList[context.currNode].Transition(input);
        context.currNode = -1;
    }

    protected override bool DoEvalute(WorkData input)
    {
        var context = GetContext<NodeDataPriority>(input);
        for (int i = 0; i < childList.Count; i++)
        {
            if (childList[i].Evaluate(input))
            {
                if (i != context.currNode)
                {
                    Transition(input);
                    context.currNode = i;
                }
                return true;
            }
        }
        context.currNode = -1;
        return false;
    }
}

public class NodeDataPriority : NodeData
{
    public int currNode = -1;
}