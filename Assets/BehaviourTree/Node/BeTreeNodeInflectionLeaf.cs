﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeTreeNodeInflectionLeaf :BeTreeNodeLeaf {

    private int behaviourID;
    public BeTreeNodeInflectionLeaf(int behaviourID) : base()
    {
        this.behaviourID = behaviourID;
    }

    protected override void Exit(WorkData input, NodeStatusEnum status)
    {
        input.GetBehaviour(behaviourID).Exit(input, status);
    }

    protected override NodeStatusEnum Update(WorkData input)
    {
        return input.GetBehaviour(behaviourID).Update(input);
    }

    protected override void Enter(WorkData input)
    {
        input.GetBehaviour(behaviourID).Update(input);
    }
}

