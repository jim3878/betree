﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BeTreeNodeNoPrioritySelector : BeTreeNodePrioritySelector
{
    
    public BeTreeNodeNoPrioritySelector() : base()
    {
        
    }

    protected override bool DoEvalute(WorkData input)
    {
        var context = GetContext<NodeDataPriority>(input);
        
        for (int i = 0; i < childList.Count; i++)
        {
            if (childList[i].Evaluate(input))
            {
                context.currNode = i;
                return true;
            }
        }
        context.currNode = -1;
        return false;
    }
}
