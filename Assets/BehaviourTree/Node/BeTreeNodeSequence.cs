﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeTreeNodeSequence : BeTreeNode
{
    public BeTreeNodeSequence( ) : base(16)
    {
    }

    public override void Transition(WorkData input)
    {
        var context = GetContext<NodeDtatSequence>(input);
        childList[context.currNode].Transition(input);
        context.currNode = 0;
    }

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        var context = GetContext<NodeDtatSequence>(input);
        var nodeStatus = childList[context.currNode].Excute(input);
        //Debug.Log(string.Format("childCount{2}  currNode{0}  nodeStatus{1}", childList[context.currNode], nodeStatus,childList.Count));
        if (nodeStatus == NodeStatusEnum.FINISH)
        {
            //判斷是否還有下一個子節點可用
            if (context.currNode < childList.Count - 1)
            {
                context.currNode++;
                return childList[context.currNode].Excute(input);
            }else
            {
                context.currNode = 0;
            }
        }
        
        return nodeStatus;
    }

    protected override bool DoEvalute(WorkData input)
    {
        var context = GetContext<NodeDtatSequence>(input);
        return childList[context.currNode].Evaluate(input);
    }
}

public class NodeDtatSequence : NodeData
{
    public int currNode=0;
}