﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BeTreeNode
{
    private int maxChildCount;
    protected string DebugName;
    protected NodeStatusEnum currStatus;
    protected bool canExcute = false;
    protected BeTreeNode parent;
    private List<BeTreeNode> _childList = new List<BeTreeNode>();
    public List<BeTreeNode> childList
    {
        get
        {
            return _childList;
        }
    }

    public void SetDebugName(string debugName)
    {
        this.DebugName = debugName;
    }

    public BeTreeNode(int maxChildCount)
    {
        this.maxChildCount = maxChildCount;
        
    }
    
    public bool Evaluate(WorkData input)
    {
        return DoEvalute(input);
    }

    public NodeStatusEnum Excute(WorkData input )
    {
        currStatus = DoExcute(input);
        return currStatus;
    }

    public void AddChild(BeTreeNode child)
    {
        if (_childList.Count >= maxChildCount)
        {
            Debug.LogError("child count reach max.");
        }
        
        _childList.Add(child);
    }

    protected T GetContext<T>(WorkData input) where T : NodeData, new()
    {
        return input.GetContext<T>(GetHashCode());
    }
    

    protected abstract bool DoEvalute(WorkData input);
    protected abstract NodeStatusEnum DoExcute(WorkData input);
    public abstract void Transition(WorkData input);


}

public enum NodeStatusEnum
{
    READY,
    RUNNING,
    FINISH,
    TRANSITION,
    ERROR,
}