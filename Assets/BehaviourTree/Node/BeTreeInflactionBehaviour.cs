﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IBeTreeInflactionBehaviour
{

    protected T GetContext<T>(WorkData input) where T : NodeData, new()
    {
        return input.GetContext<T>(GetHashCode());
    }

    public virtual void Enter(WorkData input) { }
    public virtual NodeStatusEnum Update(WorkData input) { return NodeStatusEnum.FINISH; }
    public virtual void Exit(WorkData input, NodeStatusEnum status) { }

}
