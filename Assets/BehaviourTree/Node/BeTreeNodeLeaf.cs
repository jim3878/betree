﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BeTreeNodeLeaf : BeTreeNode
{

    public BeTreeNodeLeaf( ) : base(0)
    {
    }

    public override void Transition(WorkData input)
    {
        var context = GetContext<NodeDataLeaf>(input);
        if (context.needExit)
        {
            Exit(input,NodeStatusEnum.TRANSITION);
        }
        context.status = NodeStatusEnum.READY;
        context.needExit = false;
    }

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        var context = GetContext<NodeDataLeaf>(input);
        var status = NodeStatusEnum.FINISH;
        if (context.status == NodeStatusEnum.READY)
        {
            status = NodeStatusEnum.RUNNING;
            context.status = status;
            Enter(input);
            context.needExit = true;
        }
        if (context.status == NodeStatusEnum.RUNNING)
        {
            status = Update(input);
            context.status = status;
        }
        if (context.status == NodeStatusEnum.FINISH)
        {
            Debug.Log("finish");
            if (context.needExit)
            {
                Debug.Log("exit");
                Exit(input,context.status);
                context.needExit = false;
            }
            context.status = NodeStatusEnum.READY;
        }
        return status;
    }

    protected override bool DoEvalute(WorkData input)
    {
        return true;
    }

    protected T GetUserData<T>(WorkData input) where T : class, new()
    {
        return GetContext<NodeDataLeaf>(input).GetUserData<T>();
    }
    protected virtual void Enter(WorkData input) { }
    protected virtual NodeStatusEnum Update(WorkData input) { return NodeStatusEnum.FINISH; }
    protected virtual void Exit(WorkData input,NodeStatusEnum status) { }
}


public class NodeDataLeaf : NodeData
{
    public NodeStatusEnum status = NodeStatusEnum.READY;
    public bool needExit=false;
    private object userData;
    public T GetUserData<T>() where T : class, new()
    {
        if (userData == null)
        {
            userData = new T();
        }
        return userData as T;
    }
}

