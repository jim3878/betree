﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkData  {

    private static List<IBeTreeInflactionBehaviour> inflectionBehaviourList = new List<IBeTreeInflactionBehaviour>();
    private Dictionary<int,NodeData> contex=new Dictionary<int, NodeData>();
    private Dictionary<int, IBeTreeInflactionBehaviour> behaviourLib = new Dictionary<int, IBeTreeInflactionBehaviour>();

    public void AddBehaviour<T>(int behaviourID) where T : IBeTreeInflactionBehaviour, new()
    {
        var len = inflectionBehaviourList.Count;
        int behaviourIndex = -1;

        for(int i = 0; i < len; i++)
        {
            if(inflectionBehaviourList[i] is T)
            {
                behaviourIndex = i;
                break;
            }
        }
        if (behaviourIndex == -1)
        {
            behaviourIndex = inflectionBehaviourList.Count;
            inflectionBehaviourList.Add(new T());
        }
        var behaviour = inflectionBehaviourList[behaviourIndex];
        behaviourLib.Add(behaviourID, behaviour);
    }

    public IBeTreeInflactionBehaviour GetBehaviour(int behaviourID)
    {
        if (behaviourLib.ContainsKey(behaviourID))
        {
            return behaviourLib[behaviourID];
        }
        throw new System.ArgumentNullException("Behaviour not exist .ID = " + behaviourID);
    }

    public T GetContext<T>(int uniqueID) where T:NodeData,new()
    {
        if (!contex.ContainsKey(uniqueID))
        {
            contex.Add(uniqueID, new T());
        }
        return contex[uniqueID] as T;
    }
}

public class NodeData
{

}
