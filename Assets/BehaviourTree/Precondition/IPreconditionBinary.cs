﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IPreconditionBinary : IPreconditionNode
{
    protected IPrecondition lhs, rhs;
    public IPreconditionBinary(IPrecondition lhs, IPrecondition rhs,BeTreeNode child) : base(child)
    {
        this.lhs = lhs;
        this.rhs = rhs;
        AddChild(child);
    }

    public override void Transition(WorkData input)
    {
        childList[0].Transition(input);
    }

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        return childList[0].Excute(input);
    }
}
