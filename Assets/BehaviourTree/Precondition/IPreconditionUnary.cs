﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IPreconditionUnary : IPreconditionNode
{
    protected IPrecondition condition;

    public IPreconditionUnary(IPrecondition condition, BeTreeNode child) : base(child)
    {
        this.condition = condition;
    }
}
