﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Precondition : IPreconditionUnary
{
    public Precondition(IPrecondition condition, BeTreeNode child) : base(condition, child)
    {
    }

    protected override bool DoEvalute(WorkData input)
    {
        return condition.Condition(input);
    }
}
