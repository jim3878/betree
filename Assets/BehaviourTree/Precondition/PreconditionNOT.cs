﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreconditionNOT : IPreconditionUnary
{
    public PreconditionNOT(IPrecondition condition,BeTreeNode child) : base(condition,child) { }
    

    protected override bool DoEvalute(WorkData input)
    {
        return !condition.Condition(input);
    }
}
