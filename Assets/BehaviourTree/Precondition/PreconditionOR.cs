﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreconditionOR : IPreconditionBinary
{
    public PreconditionOR(IPrecondition lhs, IPrecondition rhs, BeTreeNode child) : base(lhs, rhs, child)
    {
    }

    protected override bool DoEvalute(WorkData input)
    {
        return lhs.Condition(input) || rhs.Condition(input);
    }
}
