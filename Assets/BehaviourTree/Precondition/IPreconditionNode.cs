﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IPreconditionNode : BeTreeNode
{
    public IPreconditionNode(BeTreeNode child ) : base(1)
    {
        AddChild(child);
    }

    public override void Transition(WorkData input)
    {
        childList[0].Transition(input);
    }

    protected abstract override bool DoEvalute(WorkData input);

    protected override NodeStatusEnum DoExcute(WorkData input)
    {
        return childList[0].Excute(input);
    }
}
