﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreconditionTRUE : IPreconditionUnary
{
    public PreconditionTRUE( BeTreeNode child) : base(null, child)
    {
    }

    protected override bool DoEvalute(WorkData input)
    {
        childList[0].Evaluate(input);
        return true;
    }
}
