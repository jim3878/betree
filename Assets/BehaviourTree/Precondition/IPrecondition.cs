﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IPrecondition
{
    public IPrecondition()
    {
    }

    protected T GetContext<T>(WorkData input) where T :NodeData, new()
    {
        return input.GetContext<T>(GetHashCode());
    }

    public abstract bool Condition(WorkData input);
}
